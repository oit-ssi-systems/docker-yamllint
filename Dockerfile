FROM alpine:3.14
# hadolint ignore=DL3018
RUN apk add --no-cache yamllint && \
    wget https://git-internal.oit.duke.edu/oit-ssi-systems/duke-vault-tool/-/releases/v0.8.0/downloads/duke-vault-tool_0.8.0_linux_amd64.apk && \
    apk add --no-cache --allow-untrusted duke-vault-tool_0.8.0_linux_amd64.apk && \
    rm -f duke-vault-tool_0.8.0_linux_amd64.apk
